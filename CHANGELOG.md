# PteroJS Panel Changelog

#### 1.0.0 - Major Update
------
* v1 is now released.
* @theMackabu has joined the developer team, welcome aboard. :wave:
* PteroJS Panel (formerly: OSMP) now uses Grayshift CSS instead of Materalized Bootstrap.
* Branch themes into their own branches.
* Add Discord OAuth2
* Add Settings Page
* Update README
* Redesign most (if not all) pages.
* Add try/catch/debug functionality that can be changed in .env.

#### 0.0.2 - Remove annoying debugging messages and fix auth issue.
------
* Forgot that I had some pretty annoying debug messages in the code, this removes them.
* If user left the panel then came back and was still authenticated, they would see some user data instead of the dashbaord.

#### 0.0.1 - Inital Version
------
* Stopping servers does not work yet.
* Everything else should work, but if it doesn't, open a issue.

