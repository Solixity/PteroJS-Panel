# PteroJS Panel
PteroJS Panel is a panel/dashboard dedicated to management of any Minecraft Server, ranging from Vanilla all the way to Forge.

## Features
* Limited Server Access
* Player List
* Eye Friendly (Automatic Light/Dark Mode)
* Recommendations to boost server perf.
* 90% Whitelabeled
* Two Click Server Reinstallation
* Constant Updates

## Requirements
* NodeJS v14 (v16 has more issues but can still be used)
* 512 MB RAM
* Dual Core CPU @ 1 GHz
* MongoDB Cluster/Server
* Time, Patience, and half a cup of coffee.

## Installation
Each theme has their own branch and can be downloaded from there. \
[Grayshift (Advanced Functionality)](https://github.com/Solixity/PteroJS-Panel/tree/grayshift-advanced) \
[Grayshift (Simple Functionality) (Legacy)](https://github.com/Solixity/PteroJS-Panel/tree/grayshift)
## Support
Any support needed for installing/debugging the panel can be provided in the official [Discord Server](https://discord.gg/uzcQZNsysK). \
Not everything is 100% safe, especially when it comes to Web Applications. If you find a vulenerability, join the server and notify one of the developers in __PRIVATE__ immediately. The sooner it comes to our attention, the faster we can get a fix out for it.

## Changelog & Preview
Read the changelog [here](https://github.com/Solixity/PteroJS-Panel/blob/main/CHANGELOG.md). \
and view some screenshots [here](https://github.com/Solixity/PteroJS-Panel/blob/main/SCREENSHOTS.MD). \
Demo is available in the [Discord Server](https://discord.gg/uzcQZNsysK).
